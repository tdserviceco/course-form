app.controller('AdminController', ['$scope','$http','$window','$cookies' ,function($scope, $http ,$window,$cookies) {
  if($cookies.get('userActive') === null || $cookies.get('userActive') === undefined){
    $scope.displayLoginIssue = true;
    $scope.loadingScreen = false;
    $scope.displayResult = true;
    setTimeout(function(){ $window.location.href = '/' }, 3000);

  }
  else {
    $scope.displayLoginIssue = false;
    $scope.loadingScreen = true;
    $scope.displayResult = true;
    $http.get("../php/admin-panel.php")
    .then(function(response) {
      $scope.loadingScreen = false;
      $scope.displayResult = false;
      $scope.table = response.data;
      $scope.username = $cookies.get('userActive');
    });

    $scope.logOut = function() {
      $cookies.remove('userActive');
      $window.location.href = '/';
    }
  }

}]);
