app.controller('FormCtrl', ['$scope','$http','$window' ,function($scope,$http, $window) {
  $scope.participant = [];
  $scope.x = 0;
  $scope.hide = true;
  $scope.hideButton = true;

  // FETCH JSON FILE
  $http.get("endpoints/courses.json")
  .then(function(response) {
    $scope.form = response.data;
  });

  $scope.updateCourse = function() {
    if($scope.selectedCourse){
      $scope.dateToCourse = $scope.selectedCourse.dates;
      $scope.idForCourse = $scope.selectedCourse.id;
    };
  };

  $scope.updateTime = function() {
    if($scope.updateTime !== null){
      $scope.dateFromCourse = $scope.dateFromCourse;
    };
  };

  $scope.addNewParticipant = function(){

    $scope.x++;
    //ADD NEW PARTICIPANTS
    $scope.participant.push({});
    if($scope.participant.length >= 2){
      $scope.hideButton = false;
    }
  };

  $scope.removeParticipant = function(){
    $scope.x--;
    $scope.participant.pop();
    if($scope.participant.length === 1) {
      $scope.hideButton = true;
      return;
    }
  }

  $scope.closeBox = function(){
    $scope.hide = true;
    $window.location.reload();
  };

  $scope.submit = function() {

    if($scope.myForm.$valid){
      JSON.stringify($scope.participant);

      var data = {
        'id' : $scope.selectedCourse.id,
        'course_name' : $scope.selectedCourse.name,
        'time' : $scope.dateFromCourse,
        'company_name' : $scope.cName,
        'company_phone' : $scope.cPhone,
        'company_email' : $scope.cEmail,
        'participants' : $scope.participant,
        'total' : $scope.x
      };

      $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

      $http({
        method: 'POST',
        url: 'php/db.php',
        data: data
      }).then(function successCallback(response) {
        // console.log(response.data);
      }, function errorCallback(error) {
        console.log(error)
      });
      document.getElementById('courseForm').reset();
      $scope.hide = false;
    }
    else {
      if($scope.participant.length === 0) {
        $scope.addNewParticipant();
      }
      alert("Please fill all boxes")
    }
  }
}]);
