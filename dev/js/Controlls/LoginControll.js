app.controller('LoginController', ['$scope','$http','$window','$cookies' ,function($scope, $http ,$window,$cookies) {
  if($cookies.get('userActive')){
    $window.location.href = '/panel';
  }
  else {
    $scope.login = function(){
      if($scope.loginForm.$valid) {
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

        var data = {
          'name' : $scope.username,
          'password' : $scope.password
        }

        $http({
          method: 'POST',
          url: '../php/login-checkup.php',
          data: data
        }).then(function successCallback(response) {

          if(response.data === 'PASSED') {
            $window.location.href = '/panel';
            $cookies.put('userActive', $scope.username);
          }

          else {
            alert(response.data);
          }

        }, function errorCallback(error) {
          console.log(error)
        });
      }
      else {
        alert("Fill all!");
      }
    }
  }
}])
