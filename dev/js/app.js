@import "../bower_components/jquery/dist/jquery.js";
@import "../bower_components/angular/angular.js";
@import "../bower_components/angular-route/angular-route.js";
@import "../bower_components/angular-resource/angular-resource.js"
@import "../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js";
@import "../node_modules/angular-sidebarjs/dist/angular-sidebarjs.js";
@import "../bower_components/angular-sanitize/angular-sanitize.js";
@import "../bower_components/angular-cookies/angular-cookies.js";

'use strict';
+function runThemeApp() {

  var appDependencies = [
    'ngRoute',
    'ngResource',
    'ngSanitize',
    'ngCookies'
  ];

  var app = angular.module('angularApp',appDependencies)

  //Filters

  //Controlls
  @import "Controlls/FormControll.js";
  @import "Controlls/LoginControll.js";
  @import "Controlls/AdminControll.js";

  //Directives

  //Routes
  @import "routes.js";
}();
