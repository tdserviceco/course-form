app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: true
  });

  $routeProvider
  .when("/", {
    templateUrl : "/views/frontpage/frontpage.html",
    controller : "FormCtrl"

  })
  .when("/login", {
    templateUrl : "views/admin/login.html",
    controller : "LoginController"
  })
  .when("/panel", {
    templateUrl : "views/admin/admin-panel.html",
    controller : "AdminController"
  })
  .otherwise({
    template : "<div class='container'><h1>404 error</h1></div>"
  });
}]);
