<?php
include_once('setup-DB.php');
class user_session extends db_setup {

  static function init() {
    $conn = mysqli_connect(HOST,USER,PASSWORD,DB);
    self::getCourses($conn);
    $conn->close();
  }

  static function getCourses($conn) {
    $sql = "SELECT * FROM courses";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) :
      // output data of each row
      $value="<table>
      <tr>
      <th>Course ID</th>
      <th>Course Name</th>
      <th>Company</th>
      <th>Company Email</th>
      <th>Company Phone nr </th>
      <th>People Attending</th>
      </tr>
      ";
      while($row = $result->fetch_assoc()) :
        $value .= "
        <tr>
        <td>".$row['course_id']."</td>
        <td>".$row['course_name']."</td>
        <td>".$row['company_name']."</td>
        <td>".$row['company_email']."</td>
        <td>".$row['company_phone']."</td>
        <td>".$row['total']."</td>
        </tr>";
      endwhile;
      $value.="</table>";
      echo $value;
      else :
      echo "0 results";
    endif;
  }
}

user_session::init();
?>
