<?php
include_once('setup-DB.php');
/**
* Sending ajax $_POST from FormControll.js to here and send the $_POST
* information to DB
**/

class db extends db_setup
{

  static function init(){
    self::run_db();
  }

  static function run_db() {

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    // echo print_r($request,1);
    $peoples = json_encode($request->participants);
    $conn = mysqli_connect(HOST,USER,PASSWORD,DB);

    self::add_admin($conn);
    self::add_participants($conn,$request,$peoples);
    $conn->close();

  }

  static function add_admin($conn){

    /**
    ** CREATING FAKE ACCOUNT FOR ADMIN
    */

    $admin = "CREATE TABLE admin (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      name VARCHAR(30) NOT NULL,
      password VARCHAR(30) NOT NULL,
      reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )";

    if ($conn->query($admin) === TRUE) {
      echo "TABLE ADMIN CREATED \n\r";

      if ($result = $conn->query("SHOW TABLES LIKE 'admin'")) {
        if($result->num_rows == 1) {
          echo "CONTINUE... \n\r";
          $sql = "INSERT INTO admin (name, password)
          VALUES ('root','root')";

          if ($conn->query($sql) === TRUE) {
            echo "ADMIN CREATED \n\r";
          } else {
            echo "ERROR:" . $conn->error ."\n\r";
          }
        }
      }
    } else {
      echo "ADMIN TABLE EXIST.. NO NEED TO WORRY ABOUT ADMIN\n\r";
    }
  }

  static function add_participants($conn,$request,$peoples){

    /**
    * VIA AJAX GET PARTICIPANTS
    * AND ADD THEM TO TABLE COURSE
    */

    $company = array(
      'id' => $request->id,
      'courseName' => $request->course_name,
      'companyName' => $request->company_name,
      'companyPhone' => $request->company_phone,
      'companyEmail' => $request->company_email,
      'time' => $request->time,
      'participants' => $peoples,
      'total' => $request->total
    );


    // Check connection
    if (mysqli_connect_errno()):
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
    endif;

    // sql to create table
    $courses = "CREATE TABLE ".DB_TABLE." (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      course_id INT(10) NOT NULL,
      course_name LONGTEXT NOT NULL,
      company_name VARCHAR(30) NOT NULL,
      company_phone VARCHAR(30) NOT NULL,
      company_email VARCHAR(50),
      participants LONGTEXT NOT NULL,
      total INT(30) NOT NULL,
      time_date LONGTEXT NOT NULL,
      booked_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
      )";


      if ($conn->query($courses) === TRUE) :
        echo "TABLE ".DB_TABLE." CREATED SUCCESSFULLY \n\r";
        if ($result = $conn->query("SHOW TABLES LIKE '".DB_TABLE."'")) :
          if($result->num_rows == 1) :
            /**
            * IF TABLE EXIST SEND THE INFORMATION TO TABLE
            */
            echo "CONTINUE WITH SENDING INFORMATION...\n\r";

            $sql = "INSERT INTO ".DB_TABLE." (course_id, course_name, company_name, company_phone, company_email, participants, total, time_date)
            VALUES ('".$company['id']."','".$company['courseName']."','".$company['companyName']."','".$company['companyPhone']."','".$company['companyEmail']."','".$company['participants']."','".$company['total']."','".$company['time']."')";

            if ($conn->query($sql) === TRUE) :
              echo "INFORMATION STORED!";
             else :
              echo "Error: " . $sql . "
              " . $conn->error;
            endif;

          endif;
        endif;
       else :
        echo "TABLE COURSES EXIST \n\r";
        echo "CONTINUE WITH SENDING INFORMATION... \n\r";
        $sql = "INSERT INTO ".DB_TABLE." (course_id, course_name, company_name, company_phone, company_email, participants, total, time_date)
        VALUES ('".$company['id']."','".$company['courseName']."','".$company['companyName']."','".$company['companyPhone']."','".$company['companyEmail']."','".$company['participants']."','".$company['total']."','".$company['time']."')";

        if ($conn->query($sql) === TRUE) :
          echo "INFORMATION STORED!";
        else :
          echo "Error: " . $sql . "
          " . $conn->error;
        endif;
      endif;
  }

}

db::init();
?>
