<?php
/**
 * SETTING UP DB
 */
class db_setup
{
  static function database(){
    define("HOST", "localhost", true);
    define("USER", "root", true);
    define("PASSWORD", "root", true);
    define("DB", "local", true);
    define("DB_TABLE", "courses", true);
  }
}

db_setup::database();
?>
