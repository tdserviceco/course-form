#Course Form

#This was for a test to see how I work as a web developer.

#How to use this project:

To change Database information go to php folder and then to 
```
#!php

setup-db.php 
```


If you don´t have GRUNT or BOWER globally installed in your computer you get them here:
```
#!npm

npm install -g grunt-cli
npm install -g bower

```

To use npm you need nodeJS installed (https://nodejs.org/en/download/) (I used version 6.0.4 in this project)


Grunt: https://gruntjs.com/ for more info on Grunt
Bower: https://bower.io/ for more info on Bower


I have in my dev folder a script ready to use so you get bower and grunt installed without typing anything.. just go to cd /path-to-your-folder/dev/ and then type
```
#!node

npm run setup
```


To activate grunt with all the settings intact use command inside the dev folder:
```
#!node

grunt


```
When finished and want to go production mode then you close grunt and type:
```
#!node

grunt dist


```
it will compile all js/scss/etc to minified of the js/css.

You don't need to use the Grunt/bower stuff to see this project just incase you want to mess around and see how it works.

#Check the fake admin page

Site to the fake admin page is: *domain*/login and USER/PASSWORD is: root/root (try also to see what happens if you insert wrong user/password).